<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">

    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <link rel="stylesheet" href="../assets/css/styles.css">

    <link rel="apple-touch-icon" href="../assets/img/favicon/favicon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon/favicon.png">

    <title>Meine erste Webprogrammierung</title>
</head>
<body>
<div class="container" id="mainNavigation">
    <div class="row">
        <div class="col">
            <ul class="tabbar">
                <li class="<?= str_contains($_SERVER['PHP_SELF'], "profile") ? "active" : ""; ?>">
                    <a href="/pages/profile.php">Profil</a>
                </li>
                <li class="<?= str_contains($_SERVER['PHP_SELF'], "guestbook") ? "active" : ""; ?>">
                    <a href="/pages/guestbook.php">Gästebuch</a>
                </li>
            </ul>
        </div>
    </div>
</div>
