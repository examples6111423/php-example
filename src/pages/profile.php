<?

class Person
{

    public $id = -1;
    public $name = "n.v.";
    public $birthday = "n.v.";
    public $phone = "n.v.";
    public $email = "n.v.";


    public function load()
    {
        $dbConn = $this->_dbConnection();

        $dbPerson = pg_query($dbConn, "SELECT * FROM profile");

        while ($row = pg_fetch_row($dbPerson)) {
            $this->id = $row[0];
            $this->name = $row[1];
            $this->birthday = $row[2];
            $this->phone = $row[3];
            $this->email = $row[4];
        }
    }

    public function save()
    {
        if (isset($_POST["id"])) {
            $dbConn = $this->_dbConnection();
            $rawQuery = "UPDATE profile SET name = '%s', birthday = '%s', phone = '%s', email = '%s' WHERE id = %d";
            $query = sprintf($rawQuery, $_POST['name'], $_POST['birthday'], $_POST['phone'], $_POST['email'], $_POST['id']);
            $res = pg_query($dbConn, $query);
        }
    }

    private function _dbConnection()
    {
        $con = pg_connect("host=host.docker.internal dbname=profile_page user=profile-page password=8ac76b9b-3e2e-4d9c-8caa-ecb7788966bb")
            or die('Could not connect: ' . pg_last_error());
        return $con;
    }


}

$person = new Person();

if (isset($_POST["id"])) {
    $person->save();
}


$person->load();



?>
<? include "../template/header.php" ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="border-bottom">Das bin Ich</h1>
            <p id="coloredText">
                Hallo, das bin Ich. Auf dieser Seite erfährst Du mehr über mich.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <img src="../assets/img/Schmitz_Axel_2023.jpg" alt="Axel Schmitz" />
        </div>
        <div class="col">
            <div class="row border-bottom" style="margin-bottom: 10px">
                <div class="col nm">
                    <h2 style="margin-bottom: 5px">Meine Daten</h2>
                </div>
                <div class="col nm right-align">
                    <h2 style="margin-bottom: 5px; cursor: pointer;"
                        onclick="toogleElements('personTable', 'personForm')">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-pencil-square" viewBox="0 0 20 20">
                            <path
                                d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                            <path fill-rule="evenodd"
                                d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z" />
                        </svg>
                    </h2>
                </div>
            </div>
            <table id="personTable">
                <tr>
                    <td>Name</td>
                    <td class="align-right">
                        <?= $person->name; ?>
                    </td>
                </tr>
                <tr>
                    <td>Geburtsdatum</td>
                    <td class="align-right">
                        <?= $person->birthday; ?>
                    </td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td class="align-right">
                        <?= $person->phone; ?>
                    </td>
                <tr>
                    <td>E-Mail</td>
                    <td class="align-right">
                        <?= $person->email; ?>
                    </td>
                </tr>
            </table>
            <form method="POST" action="<?= $_SERVER['PHP_SELF']; ?>" class="hidden" id="personForm">
                <input type="hidden" name="id" value="<?= $person->id; ?>" />
                <table>
                    <tr>
                        <td>Name</td>
                        <td class="align-right">
                            <input type="text" name="name" value="<?= $person->name; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>Geburtsdatum</td>
                        <td class="align-right">
                            <input type="text" name="birthday" value="<?= $person->birthday; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>Telefon</td>
                        <td class="align-right">
                            <input type="text" name="phone" value="<?= $person->phone; ?>" />
                        </td>
                    <tr>
                        <td>E-Mail</td>
                        <td class="align-right">
                            <input type="text" name="email" value="<?= $person->email; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="align-right">
                            <input type="submit" value="Speichern" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<? include "../template/footer.php" ?>