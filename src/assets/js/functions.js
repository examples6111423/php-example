
function toogleElements(idToShow, idToHide) {

    document.getElementById(idToShow).classList.toggle("hidden");
    document.getElementById(idToHide).classList.toggle("hidden");

}


function randomizeColorText() {
    let elem = document.getElementById("coloredText");
    let words = elem.innerText.split(" ");
    let newString = "";

    for (let i = 0; i < words.length; i++) {
        let randomColor = Math.floor(Math.random()*16777215).toString(16);
        newString += '<span style="color: #' + randomColor + '">' + words[i] + ' </span>';
    }

    elem.innerHTML = newString;
}


randomizeColorText();

window.setInterval(() => {
    randomizeColorText();
}, 1000)
